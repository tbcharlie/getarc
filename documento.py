from docxtpl import DocxTemplate

doc = DocxTemplate("te.docx")
context = {'company_name': "World company"}
doc.render(context)
doc.save("tres.docx")
